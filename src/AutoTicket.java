import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class AutoTicket extends JFrame {

    private JTextField posX, posY = null;
    private JTextField color = null;
    
    private JButton btnDemo = null;
    private volatile boolean stop = true;

    public AutoTicket() {
        super("AutoClick");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setBounds(60, 60, 500, 360);
        getContentPane().setLayout(null);
        this.posX = new JTextField("800");
        this.posX.setBounds(30, 30, 120, 36);
        this.posX.setToolTipText("X position");
        this.posY = new JTextField("540");
        this.posY.setToolTipText("Y position");
        this.posY.setBounds(30, 100, 120, 36);
        this.color = new JTextField("Color");
        this.color.setToolTipText("Cursor color");
        this.color.setBounds(30, 150, 300, 36);

        this.btnDemo = new JButton("Start");
        this.btnDemo.setBounds(350, 250, 76, 23);
        this.btnDemo.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                startDemo();
            }
        });
        getContentPane().add(this.posX);
        getContentPane().add(this.posY);
        getContentPane().add(this.color);
        getContentPane().add(this.btnDemo);
        getContentPane().addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    stop = true;
                    System.exit(0);
                }
            }

        });
    }

    public void startDemo() {
        stop = !stop;
        this.btnDemo.setText(stop ? "Start" : "Stop");
        if (stop)
            return;
        Thread rt = new Thread(new Runnable() {
            public void run() {
                try {
                    Robot rbt = new Robot();
                    Point p = new Point(Integer.valueOf(posX.getText()), Integer.valueOf(posY.getText()));
                    rbt.delay(100);
                    while (!stop) {
                        rbt.mouseMove(p.x, p.y);
                        rbt.delay(100);
                        //rbt.mousePress(InputEvent.BUTTON1_MASK);
                        rbt.delay(100);
                        //rbt.mouseRelease(InputEvent.BUTTON1_MASK);
                        Color c = rbt.getPixelColor(p.x, p.y);
                        color.setText(c+"");
                        if ((c.getRed() - c.getGreen()) >= 100){
                            stop = true;
                            btnDemo.setText(stop ? "Start" : "Stop");
                        }
                    }
                } catch (AWTException e) {
                    e.printStackTrace();
                }
            }
        });
        rt.start();
    }

    public static void main(String[] args) {
        AutoTicket adf = new AutoTicket();
        adf.setVisible(true);
    }

}
